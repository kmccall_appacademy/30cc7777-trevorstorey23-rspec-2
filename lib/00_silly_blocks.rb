def reverser
  words = yield.split(" ")
  reverse_words = words.map { |word| word.reverse }
  reverse_words.join(" ")
end

def adder(num = 1)
  yield + num
end

def repeater(num = 1)
  num.times {yield}
end
